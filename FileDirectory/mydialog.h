#ifndef MYDIALOG_H
#define MYDIALOG_H

#include <QDialog>

class myDialog : public QDialog
{
Q_OBJECT
public:
    myDialog(QWidget* parent=nullptr);

private slots:
    void onCreate();
    void onRemove();
private:
    void setupConnections();

    class Private;
    Private* const d;
};

#endif // MYDIALOG_H
