#include "mydialog.h"

#include <QDirModel>
#include <QTreeView>
#include <QVBoxLayout>
#include <QPushButton>
#include <QModelIndex>
#include <QDebug>
#include <QHeaderView>
#include <QFileSystemModel>
#include <QInputDialog>
#include <QMessageBox>

class myDialog::Private{
public:
    Private():
        model(nullptr),
        treeView(nullptr),
        createButton(nullptr),
        removeButton(nullptr),
        quitButton(nullptr)
    {
    }
    QFileSystemModel* model;
    QTreeView* treeView;
    QPushButton* createButton;
    QPushButton* removeButton;
    QPushButton* quitButton;
};


myDialog::myDialog(QWidget* parent):
    QDialog(parent),
    d(new Private())
{
    resize(800,400);
    d->model = new QFileSystemModel;
    /*
     * The Root Path, controls which Directories,
     * are being watched. Any updates to the folder system,
     * will be auto. reflected in our program.
     */
    d->model->setRootPath("/home/kartik");
    d->model->setReadOnly(false);

    d->treeView = new QTreeView(this);
    d->treeView->setModel(d->model);
    d->treeView->setRootIndex(d->model->index("/home/kartik"));
    d->treeView->setSortingEnabled(true);

    d->treeView->header()->setStretchLastSection(true);
    /*
     * You can use this to manually
     * resize your sections
       d->treeView->header()->resizeSection(0, 200);
    */

    /*
     * This controls the initial state of the Tree.
     * The TreeView will get sorted in Ascending Order,
     * using Column at Index 2, and Indicator for Sorting,
     * will be shown over this column.
     */
    d->treeView->header()->setSortIndicator(0, Qt::AscendingOrder);

    d->treeView->header()->setSortIndicatorShown(true);

    /*
     * Enables the Column at Index 0,
     * to Resize so that its entire contents
     * are shown.
     */
    d->treeView->header()->setSectionResizeMode(0,QHeaderView::ResizeToContents);

    d->createButton = new QPushButton("Create");
    d->removeButton = new QPushButton("Remove");
    d->quitButton = new QPushButton("Quit");

    QHBoxLayout* buttonBox = new QHBoxLayout();
    buttonBox->addWidget(d->createButton);
    buttonBox->addWidget(d->removeButton);
    buttonBox->addStretch();
    buttonBox->addWidget(d->quitButton);

    QVBoxLayout* mainLayout = new QVBoxLayout();
    mainLayout->addWidget(d->treeView);
    mainLayout->addLayout(buttonBox);
    mainLayout->setSizeConstraint(QLayout::SetNoConstraint);
    setLayout(mainLayout);

    setupConnections();
    show();
}

void myDialog::setupConnections(){
    connect(d->createButton, &QPushButton::clicked, this, &myDialog::onCreate);
    connect(d->removeButton, &QPushButton::clicked, this, &myDialog::onRemove);
}

void myDialog::onCreate(){
    QModelIndex index = d->treeView->currentIndex();
    if(!index.isValid()){
        QMessageBox::information(this, tr("Create Directory"), tr("Failed to Create Directory"));
        return ;
    }
    if(!d->model->isDir(index)){
        QMessageBox::warning(this,tr("Failed!"),tr("Please select a Directory!"));
        return;
    }

    QString dirName = QInputDialog::getText(this, "Create Directory", "Name");

    if(!dirName.isEmpty()){
        if(!d->model->mkdir(index,dirName).isValid()){
            QMessageBox::information(this, tr("Create Directory"), tr("Failed to Create Directory"));
        }
    }
}

void myDialog::onRemove(){
    QModelIndex index = d->treeView->currentIndex();
    if(!index.isValid())
        return;
    if(!d->model->isDir(index)){
        QMessageBox::warning(this,tr("Failed!"),tr("Please select a Directory!"));
        return;
    }
    if(!d->model->rmdir(index)){
        QMessageBox::warning(this,tr("Failed!"),tr("Failed to Remove Directory!"));
    }
}
