#ifndef MYDIALOG_H
#define MYDIALOG_H

#include <QDialog>

class myDialog : public QDialog
{
Q_OBJECT
public:
    myDialog(const QStringList& leaders, QWidget* parent=nullptr);

private slots:
    void onInsert();
    void onDelete();
private:
    void setupConnections();

    class Private;
    Private* const d;
};

#endif // MYDIALOG_H
