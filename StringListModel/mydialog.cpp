#include "mydialog.h"

#include <QStringListModel>
#include <QListView>
#include <QVBoxLayout>
#include <QPushButton>
#include <QModelIndex>

class myDialog::Private{
public:
    Private():
        model(nullptr),
        listView(nullptr)
    {
    }
    QStringListModel* model;
    QListView* listView;
    QPushButton* insertButton;
    QPushButton* deleteButton;
};


myDialog::myDialog(const QStringList& leaders, QWidget* parent):
    QDialog(parent),
    d(new Private())
{
    d->model = new QStringListModel;
    d->model->setStringList(leaders);

    d->listView = new QListView(this);
    d->listView->setModel(d->model);
    d->listView->setEditTriggers(QAbstractItemView::AnyKeyPressed|QAbstractItemView::DoubleClicked);

    d->insertButton = new QPushButton("Insert");
    d->deleteButton = new QPushButton("Delete");
    QHBoxLayout* buttonBox = new QHBoxLayout();
    buttonBox->addWidget(d->insertButton);
    buttonBox->addWidget(d->deleteButton);
    QVBoxLayout* mainLayout = new QVBoxLayout();
    mainLayout->addWidget(d->listView);
    mainLayout->addLayout(buttonBox);
    setLayout(mainLayout);

    setupConnections();
}

void myDialog::setupConnections(){
    connect(d->insertButton, &QPushButton::clicked, this, &myDialog::onInsert);
    connect(d->deleteButton, &QPushButton::clicked, this, &myDialog::onDelete);
}

void myDialog::onInsert(){
    int row = d->listView->currentIndex().row();
    d->model->insertRow(row);
    d->model->setData(d->model->index(row), "AAA");
    QModelIndex index = d->model->index(row);
    d->listView->setCurrentIndex(index);
}

void myDialog::onDelete(){
    d->model->removeRow(d->listView->currentIndex().row());
}
