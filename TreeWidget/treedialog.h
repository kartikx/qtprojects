#ifndef TREE_DIALOG_H
#define TREE_DIALOG_H

#include <QDialog>
#include <QTreeWidgetItem>

class TreeDialog : public QDialog
{
    Q_OBJECT
public:
     TreeDialog(QWidget* parent);
public slots:
     void onChanged(QTreeWidgetItem* current, QTreeWidgetItem* previous);
private:
    class TreeDialogPrivate;
    TreeDialogPrivate* const d;
};

#endif // TREEDIALOG_H
