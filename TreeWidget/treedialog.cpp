#include "treedialog.h"
#include <QDebug>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QVBoxLayout>
#include <QHBoxLayout>

#include <QFont>
class TreeDialog::TreeDialogPrivate
{
public:
    explicit TreeDialogPrivate() : treeWidget(nullptr)
    {
    }
    QTreeWidget *treeWidget;
};

TreeDialog::TreeDialog(QWidget *parent) : QDialog(parent), d(new TreeDialogPrivate)
{
    d->treeWidget = new QTreeWidget();
    d->treeWidget->setHeaderLabels(QStringList() << tr("Num1") << tr("Num2"));
    d->treeWidget->setColumnCount(2);

    /*
     * You can either instantiate a
     * QList, and supply that to the
     * insertTopLevelItems.
     *
    QList<QTreeWidgetItem*> items;
    for(int i=0;i<10;i++)
        items.append(new QTreeWidgetItem(d->treeWidget, QStringList(QString("Item: %1").arg(i))));
    d->treeWidget->insertTopLevelItems(0,items);
    */

    /*
     * Or use addTopLevel,
     * to successively add
     * each new element
     */
    for (int i = 0; i < 5; i++)
        d->treeWidget->addTopLevelItem(new QTreeWidgetItem(QStringList() << QString::number(i) << QString::number(100 - 5 * i)));

    /*Instead of adding to treeWidget,
     * we may also specify the parent in the
     * constructor itself.
     */
    QTreeWidgetItem *newItem = new QTreeWidgetItem(d->treeWidget);
    newItem->setText(0, "Onetwo");
    newItem->setText(1, "threefour");

    /*Here parent is an
     * item, leads to heirarchies.
     */
    QTreeWidgetItem *alsoNewItem = new QTreeWidgetItem(newItem);
    alsoNewItem->setText(0, "One");
    alsoNewItem->setText(1, "Trees");

    /*This ensures that the newly constructed
     * item, appears after the item passed as parameter.
     */
    QTreeWidgetItem *thirdItem = new QTreeWidgetItem(d->treeWidget, newItem);
    thirdItem->setText(0, "Where");
    thirdItem->setText(1, "am i?");

    /*
     * I have option to select a particular item,
     * is displayed. By setting its Font.
     */
    thirdItem->setFont(1, QFont("Ubuntu", 11, QFont::Bold));

    /*
     * In order to create Heirarchies,
     * You still add an item to your widget,
     * however each item, may have children,
     */
    QTreeWidgetItem *myItem = new QTreeWidgetItem(QStringList(QString("SubTree1")));
    myItem->addChild(new QTreeWidgetItem(QStringList(QString("Hello1"))));
    myItem->addChild(new QTreeWidgetItem(QStringList(QString("Hello2"))));
    //    d->treeWidget->insertTopLevelItem(4,myItem);
    myItem->setExpanded(true);

    /*TODO
     * Work with sorting
     * ye ajeeb results kyu de raha
     */
    //    d->treeWidget->setSortingEnabled(true);
    //    d->treeWidget->sortItems(1, Qt::SortOrder::AscendingOrder);

    /*
     * Setting the Layout
     */
    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addWidget(d->treeWidget);
    setLayout(mainLayout);

    /*
     * Always prefer using this syntax,
     * allows for Compile Time Syntax Checking.
     */
    //    connect(d->treeWidget, &QTreeWidget::currentItemChanged,
    //            this, &TreeDialog::onChanged);
}
/* [Feature] whenever you move to a new item,
 * if the previous one had children, close them.
 */
void TreeDialog::onChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous)
{
    current->setExpanded(true);
    if (previous && previous->childCount() > 0)
    {
        previous->setExpanded(false);
    }
}
