#include "treedialog.h"

#include <QApplication>
#include <QMessageBox>
#include <QDebug>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    TreeDialog w(nullptr);
    w.show();
    return a.exec();
}
