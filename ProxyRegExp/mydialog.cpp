#include "mydialog.h"

#include <QStringListModel>
#include <QListView>
#include <QTreeView>
#include <QVBoxLayout>
#include <QPushButton>
#include <QModelIndex>
#include <QDebug>
#include <QInputDialog>
#include <QMessageBox>
#include <QFormLayout>
#include <QLineEdit>
#include <QComboBox>
#include <QSortFilterProxyModel>

class myDialog::Private{
public:
    Private():
        model(nullptr),
        listView(nullptr),
        filterInput(nullptr),
        proxyModel(nullptr),
        syntaxSelector(nullptr)
    {
    }
    QStringListModel* model;
    QListView* listView;
    QLineEdit* filterInput;
    QSortFilterProxyModel* proxyModel;
    QComboBox* syntaxSelector;
};


myDialog::myDialog(QWidget* parent):
    QDialog(parent),
    d(new Private())
{
    d->model = new QStringListModel;
    d->model->setStringList(QColor::colorNames());

    /*
     * A proxy model wraps over a standard model,
     */
    d->proxyModel = new QSortFilterProxyModel(this);
    d->proxyModel->setSourceModel(d->model);
    d->proxyModel->setFilterKeyColumn(0);

    /*
     * The list view, uses this proxy model.
     */
    d->listView = new QListView;
    d->listView->setModel(d->proxyModel);

    d->filterInput = new QLineEdit;

    d->syntaxSelector = new QComboBox;
    d->syntaxSelector->addItem(tr("Regular Expression"),QRegExp::RegExp);
    d->syntaxSelector->addItem(tr("Wildcard"),QRegExp::Wildcard);
    d->syntaxSelector->addItem(tr("Fixed String"),QRegExp::FixedString);

    QFormLayout* formLayout = new QFormLayout;
    formLayout->addRow(tr("&Filter: "), d->filterInput);
    formLayout->addRow(tr("&Pattern Syntax: "), d->syntaxSelector);

    QVBoxLayout* mainLayout = new QVBoxLayout();
    mainLayout->addWidget(d->listView);
    mainLayout->addLayout(formLayout);
    mainLayout->setSizeConstraint(QLayout::SetNoConstraint);

    setLayout(mainLayout);

    setupConnections();
}

void myDialog::reapplyFilter(){
    QRegExp::PatternSyntax syntax = QRegExp::PatternSyntax(d->syntaxSelector
                                    ->itemData(d->syntaxSelector->currentIndex())
                                    .toInt());
    QRegExp regExp(d->filterInput->text(),Qt::CaseInsensitive, syntax);
    d->proxyModel->setFilterRegExp(regExp);
}

void myDialog::setupConnections(){
    connect(d->filterInput, &QLineEdit::textChanged,this, &myDialog::reapplyFilter);
}

